package ru.bia.TimesNewRomain;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int temp;
        int sum = 0;
        System.out.println("Введите количество времени");
        int time = sc.nextInt();
        System.out.println("Введите количество заказов");
        int amount = sc.nextInt();
        int[] merchant = new int[amount];
        for (int i = 0; i < merchant.length; i++) {
            merchant[i] = (int) (Math.random() * 10);
        }
        System.out.println(Arrays.toString(merchant));
        for (int j = 0; j < merchant.length; j++) {
            for (int k = 0; k < merchant.length - 1; k++) {
                if (merchant[k] < merchant[k + 1]) {
                    temp = merchant[k];
                    merchant[k] = merchant[k + 1];
                    merchant[k + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(merchant));
        for (int i = 0; i < time; i++) {
            sum = sum + merchant[i];
        }
        System.out.println(sum);

    }
}
